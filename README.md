Brainfuck Analyser 
================== 

The goal of this program is to analyse brainfuck programs.

At least, you should be able to hover over some code, and the program 
will tell you which cell of memory it is currently on.
